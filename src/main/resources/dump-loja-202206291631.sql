--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4
-- Dumped by pg_dump version 14.4

-- Started on 2022-06-29 16:31:44

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3310 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 210 (class 1259 OID 17055)
-- Name: produtos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.produtos (
    id bigint NOT NULL,
    nome character varying NOT NULL,
    descricao character varying,
    preco double precision
);


ALTER TABLE public.produtos OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 17054)
-- Name: produtos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.produtos ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.produtos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 3304 (class 0 OID 17055)
-- Dependencies: 210
-- Data for Name: produtos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.produtos (id, nome, descricao, preco) FROM stdin;
1	Tênis Nike Esp.	Tênis Nike Esp. Tam: 43 Preto	340
2	Tênis Nike Air Force Casual	Tênis Nike Air Force 1 '07 Masculino	700
3	Top Nike Fem.	Plus Size - Top Nike Swoosh Feminino	120
6	Camiseta	Camiseta Nike Sportswear Swoosh Masculina	220
4	Camiseta Nike Infantil	Camisa Nike Portugal I 2020/21 Torcedor Infantil	140
\.


--
-- TOC entry 3311 (class 0 OID 0)
-- Dependencies: 209
-- Name: produtos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.produtos_id_seq', 6, true);


-- Completed on 2022-06-29 16:31:45

--
-- PostgreSQL database dump complete
--

