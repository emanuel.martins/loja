package br.com.proway.loja.controllers;

import br.com.proway.loja.models.Produto;
import br.com.proway.loja.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("")
public class ProdutoController {

    @Autowired
    private ProdutoRepository produtoRepository;

    @PostMapping("/produtos")
    public ResponseEntity<Produto> addProduto(@RequestBody Produto body) {
        Produto produto;

        try {
            produto = produtoRepository.save(body);

            return new ResponseEntity<>(produto, HttpStatus.CREATED);
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());

            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/produtos")
    public ArrayList<Produto> getAllProdutos() {
        return produtoRepository.findAll();
    }

    @GetMapping("/produtos/{id}")
    public Produto getProduto(@PathVariable("id") long id) {
        return produtoRepository.findById(id);
    }

    @GetMapping("/produtos/preco/{preco}")
    public ArrayList<Produto> getProdutoMaiorPreco(@PathVariable("preco") Double preco) {
        return produtoRepository.findByMoreOrEqual(preco);
    }

    @PutMapping("/produtos/{id}")
    public Produto atualizarProduto(@PathVariable("id") long id, @RequestBody Produto produto){
        produto.setId(id);
        return produtoRepository.save(produto);
    }

    @DeleteMapping("/produtos/{id}")
    public void deleteProduto(@PathVariable("id") long id){
        produtoRepository.deleteById(id);
    }
}
