package br.com.proway.loja.repositories;

import br.com.proway.loja.models.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {
    Produto save(Produto produto);
    Produto findById(long id);
    ArrayList<Produto> findAll();
    void deleteById(Long id);

    @Query("SELECT p FROM Produto p WHERE p.preco >= :preco")
    ArrayList<Produto> findByMoreOrEqual(@Param("preco") Double preco);
}
